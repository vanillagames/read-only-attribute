﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomPropertyDrawer(type: typeof(ReadOnlyAttribute))]
public class ReadOnlyAttributeDrawer : PropertyDrawer
{

    public override void OnGUI(Rect               rect,
                               SerializedProperty prop,
                               GUIContent         label)
    {
        var wasEnabled = GUI.enabled;
        
        GUI.enabled = false;

        EditorGUI.PropertyField(position: rect,
                                property: prop,
                                includeChildren: true);

        GUI.enabled = wasEnabled;
    }

}
#endif

public class ReadOnlyAttribute : PropertyAttribute { }